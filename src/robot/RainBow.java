package robot;

import java.awt.Color;
import java.awt.Graphics2D;

import robocode.*;
import robocode.util.Utils;

public class RainBow extends AdvancedRobot{

	Enemy enemic;
	boolean scanned = false;
	boolean Swinger = true;			// Segmenta el moviment "swing" (alterna ahead & back)
	double power = 1;
	final double pi = Math.PI;
	int girSwing = 75;				// Determina l'angle per al moviment "swing"
	double maxX, maxY, minX, minY;	// L�mits

	// Inici
	public void run() {

		// Color Inicial Robot
		setBodyColor(Color.MAGENTA);
		setRadarColor(Color.CYAN);
		setGunColor(Color.RED);

		enemic = new Enemy();
		
		// Definir l�mits per a la paret
		minX = minY = getHeight() * 3;
		maxX = getBattleFieldWidth() -  getHeight() * 3;
		maxY = getBattleFieldHeight() - getHeight() * 3;

		// Moviment del radar, gun i cos independents
		setAdjustGunForRobotTurn(true); 
		setAdjustRadarForGunTurn(true); 
		
		while(!scanned) {
			setAhead(200);
			setTurnLeft(77);
			setTurnRadarRightRadians(pi*2);
			execute();
		}

		while(true) {
			if(dinsLimits()) {
				if (getDistanceRemaining() == 0) swing(150);
			}
			else setAhead(30);
			execute();
		}

	}

	/**
	 * Event Robot escanejat
	 */
	@Override
	public void onScannedRobot(ScannedRobotEvent e) {
		scanned = true;

		// Calcular l'angle del robot escanejat
		double angle = Math.toRadians(beartoHead(e.getBearing()));

		setTurnRadarRightRadians(Utils.normalRelativeAngle(angle - getRadarHeadingRadians()));
		setTurnGunRightRadians(Utils.normalRelativeAngle(angle - getGunHeadingRadians()));
		
		// Calcular i guardar les coordenades del robot escanejat
		enemic.setX((int)(getX() + Math.sin(angle) * e.getDistance()));
		enemic.setY((int)(getY() + Math.cos(angle) * e.getDistance()));
		
		if (dinsLimits()) setTurnRight(e.getBearing() + girSwing);
		else goCenter();
		
		double distance = getDistance();
		
		if (distance > 300) {
			setBulletColor(Color.MAGENTA);
			power = 1;
		}
		else if (distance > 200) {
			setBulletColor(Color.ORANGE);
			power = 2;
		}
		else {
			setBulletColor(Color.RED);
			power = 3;
		}
		
		setFire(power);
	}

	/**
	 * Event xocar contra la paret
	 */
	@Override
	public void onHitWall(HitWallEvent event) {
		goCenter();
	}
	
	/**
	 * Event xocar contra un robot 
	 */
	@Override
	public void onHitRobot(HitRobotEvent event) {
		setBack(30);
	}
	
	/**
	 * Event Paint:
	 * Dibuixa els l�mits per no xocar contra la paret
	 */
	@Override
	public void onPaint(Graphics2D g) {
		g.drawLine((int)minX, (int)minY, (int)minX, (int)maxY);
		g.drawLine((int)minX, (int)minY, (int)maxX, (int)minY);
		g.drawLine((int)maxX, (int)maxY, (int)minX, (int)maxY);
		g.drawLine((int)maxX, (int)maxY, (int)maxX, (int)minY);
	}

	/**
	 * Execuci� del codi en cas de victoria
	 */
	@Override
	public void onWin(WinEvent e) {
		for (int i = 0; i < 50; i++) {
			turnRight(30);
			turnLeft(30);
		}
	}

	/**
	 * Realitza un moviment oscilatori
	 * @param pixels - dist�ncia que oscilar�
	 */
	private void swing(int pixels) {
	
		double distance = getDistance();
		
		if (distance > 300) {
			setBodyColor(Color.ORANGE);
			if(Swinger) {
				girSwing = 60;
				setAhead(pixels + 150);
				Swinger = false;
			}else{
				girSwing = 120;
				setBack(pixels + 150);
				Swinger = true;
			}
		}
		else if (distance > 200 && distance <= 300) {
			setBodyColor(Color.MAGENTA);
			if(Swinger) {
				girSwing = 80;
				setAhead(pixels + 75);
				Swinger = false;
			}else{
				girSwing = 100;
				setBack(pixels + 75);
				Swinger = true;
			}
		}
		else if (distance <= 200) {
			setBodyColor(Color.YELLOW);
			giraVolta();
		}
	}
	
	/**
	 * Determina si el robot est� dins dels l�mits predefinits [maxX, minX, maxY, minY]
	 * @return true en cas afirmatiu, en cas contrari false
	 */
	private boolean dinsLimits() {
		double x = getX();
		double y = getY();
		return (x < maxX) && (x > minX) && (y < maxY) && (y > minY);
	}
	
	/**
	 * Gira l'angle del cos del robot en direcci� al centre del Battleground
	 */
	private void goCenter() {	
		setBodyColor(Color.BLACK);
		
		// Coordenades relatives al nostre robot
		double x = getBattleFieldWidth() / 2 - getX();
		double y = getBattleFieldHeight() / 2 - getY();

		double heading = Math.toDegrees(Math.atan2(x, y));
		
		if(heading > 180) setTurnLeft(headtoBear(360 - heading));
		else setTurnRight(headtoBear(heading));
			
	}
	
	/**
	 * Realitza un moviment en forma de semi-arc
	 */
	private void giraVolta() {
		girSwing = 85;
		setAhead(getDistance() * pi);
	}

	/**
	 * Conversi� direcci� relativa (bearing) a direcci� absoluta (heading)
	 * @param bearing - direcci� relativa (bearing)
	 * @return 
	 */
	public double beartoHead(double bearing) {
		return (getHeading() + bearing) % 360;
	}

	/**
	 * Conversi� direcci� absoluta (hearing) a direcci� relativa (beading)
	 * @param bearing - direcci� absoluta (heading)
	 * @return direcci� relativa (bearing)
	 */
	public double headtoBear(double heading) {
		return (heading - getHeading()) % 360;
	}

	/**
	 * Calcula la dist�ncia entre el robot i l'enemic
	 * @return dist�ncia entre el robot i l'enemic
	 */
	public double getDistance() {
		return Math.sqrt( Math.pow(getX() - enemic.getX(), 2) + Math.pow(getY() - enemic.getY(), 2) );
	}
}
