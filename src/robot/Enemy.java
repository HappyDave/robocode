package robot;

public class Enemy {
	private double X;
	private double Y;
	
	public Enemy() {
		X = Integer.MIN_VALUE;
		Y = Integer.MIN_VALUE;
	}
	public double getX() {
		return X;
	}
	public void setX(double x) {
		X = x;
	}
	public double getY() {
		return Y;
	}
	public void setY(double y) {
		Y = y;
	}
}
